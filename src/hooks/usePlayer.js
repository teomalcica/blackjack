import { useState } from 'react';
import { BELOW, BUST, BLACKJACK } from '../constants/gameStatus';
import { useEffect } from 'react';

const usePlayer = (deck, setDeck) => {
  const [hand, setHand] = useState([]);
  const [gameStatus, setGameStatus] = useState(BELOW);
  const [isStanding, setIsStanding] = useState(false);
  const onStand = () => {
    setIsStanding(true);
  };

  useEffect(() => {
    handleHit();
  }, []);

  const resetGame = newDeck => {
    setIsStanding(false);
    setHand([newDeck[newDeck.length - 1]]);
    setGameStatus(BELOW);
  };

  const getScore = currentHand => {
    let score = 0;
    let aceCount = 0;
    currentHand.forEach(currentCard => {
      score += currentCard.value;
      if (currentCard.value === 11) {
        aceCount++;
      }
    });

    while (score > 21 && aceCount > 0) {
      aceCount--;
      score -= 10;
    }

    return score;
  };

  const handleHit = async () => {
    const newDeck = [...deck.current];
    newDeck.pop();
    setDeck(newDeck);

    const currentCard = newDeck[newDeck.length - 1];
    const newHand = [...hand, currentCard];
    setHand(newHand);
    const currentScore = getScore(newHand);

    if (currentScore > 21) {
      setGameStatus(BUST);
    }
    if (currentScore === 21) {
      setGameStatus(BLACKJACK);
    }
  };

  return {
    hand,
    gameStatus,
    getScore,
    handleHit,
    resetGame,
    isStanding,
    onStand,
  };
};

export default usePlayer;
