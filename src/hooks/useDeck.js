import cardData from '../constants/cardData';

const useDeck = () => {
  const generateDeck = () => {
    const array = [];
    const potentialCardsValues = [...cardData];

    while (potentialCardsValues.length > 0) {
      const randomNumber = Math.random() * potentialCardsValues.length;
      const currentCardIndex = Math.floor(randomNumber);
      array.push(potentialCardsValues[currentCardIndex]);
      potentialCardsValues.splice(currentCardIndex, 1);
    }

    return array;
  };

  return generateDeck;
};

export default useDeck;
