import './App.scss';
import { useState, useRef } from 'react';
import useDeck from './hooks/useDeck';
import Player from './components/player/Player';
import Dealer from './components/dealer/Dealer';
import { BELOW } from './constants/gameStatus';

const players = [
  { name: 'Mihai', score: 0, status: BELOW },
  { name: 'Maria', score: 0, status: BELOW },
  { name: 'Marius', score: 0, status: BELOW },
];

function App() {
  const generateDeck = useDeck();
  const deck = useRef(generateDeck());
  const setDeck = newDeck => (deck.current = newDeck);

  const [shouldDealerPick, setShouldDealerPick] = useState(false);

  const handleReset = () => {
    const newDeck = generateDeck();
    setDeck(newDeck);
    // resetGame(newDeck);
  };

  return (
    <div className="page-wrapper">
      {' '}
      <div className="players-wrapper">
        {players.map((player, index) => {
          return (
            <Player
              key={index}
              deck={deck}
              setDeck={setDeck}
              setShouldDealerPick={setShouldDealerPick}
            />
          );
        })}
      </div>
      <Dealer
        deck={deck}
        setDeck={setDeck}
        setShouldDealerPick={setShouldDealerPick}
        shouldDealerPick={shouldDealerPick}
      />
    </div>
  );
}

export default App;
