import './Score.scss';

const Score = ({ hand, score }) => {
  const getScoreClassName = () => {
    let className = 'hand__score';

    if (score === 21) {
      className += ' hand__score--green';
    } else if (score > 21) {
      className += ' hand__score--red';
    }

    return className;
  };

  return (
    <>
      <h1 className={getScoreClassName()}>{score}</h1>
      <div className="hand__wrapper">
        {hand.map((card, index) => (
          <img src={card.imgUrl} key={index} className="hand__image" />
        ))}
      </div>
    </>
  );
};

export default Score;
