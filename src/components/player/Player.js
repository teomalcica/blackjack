import usePlayer from '../../hooks/usePlayer';
import Controls from '../controls/Controls';
import Score from '../score/Score';
import './Player.scss';
import { useEffect } from 'react';

const Player = ({ deck, setDeck, setShouldDealerPick }) => {
  const {
    hand,
    gameStatus,
    getScore,
    handleHit,
    resetGame,
    onStand,
    isStanding,
  } = usePlayer(deck, setDeck);

  useEffect(() => {
    if (isStanding) {
      setShouldDealerPick(true);
    }
  }, [isStanding]);

  return (
    <div className="player__wrapper">
      <Score hand={hand} score={getScore(hand)} />
      <Controls
        onHit={handleHit}
        onStand={onStand}
        isStanding={isStanding}
        onReset={() => resetGame(deck)}
        gameStatus={gameStatus}
      />
    </div>
  );
};

export default Player;
