import './Deck.scss';

const Deck = props => {
  return (
    <img
      src={props.deck[props.deck.length - 1].imgUrl}
      className="card-image"
    />
  );
};
export default Deck;
