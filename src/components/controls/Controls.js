import { useState } from 'react';
import { BELOW } from '../../constants/gameStatus';
import './Controls.scss';

const Controls = ({ onHit, onReset, gameStatus, onStand, isStanding }) => {
  return isStanding || gameStatus !== BELOW ? (
    <button className="btn" onClick={onReset}>
      New Game
    </button>
  ) : (
    <div>
      <button className="btn" onClick={onHit}>
        Hit
      </button>{' '}
      <button className="btn btn--secondary" onClick={onStand}>
        Stand
      </button>
    </div>
  );
};

export default Controls;
