import { useEffect } from 'react';
import usePlayer from '../../hooks/usePlayer';
import Score from '../score/Score';

const Dealer = ({ deck, setDeck, shouldDealerPick, setShouldDealerPick }) => {
  const { hand, gameStatus, getScore, handleHit } = usePlayer(deck, setDeck);

  const score = getScore(hand);
  console.log({ score, shouldDealerPick });
  if (score < 21 && shouldDealerPick) {
    if (score > 17) {
      const probability = 1 - 0.25 * (20 - score);
      const randomNumber = Math.random();
      console.log({ score, probability, randomNumber });
      if (randomNumber > probability) {
        handleHit();
      }
    } else {
      handleHit();
    }
  }

  return (
    <div className="player__wrapper">
      <Score hand={hand} score={getScore(hand)} />
    </div>
  );
};

export default Dealer;
